function mul():void
{
    var input:HTMLInputElement=<HTMLInputElement>document.getElementById("n1");
    var table:HTMLTableElement=<HTMLTableElement>document.getElementById("t1");

    var count:number=1;
    var num:number= +input.value;   //+is a shortcut for parseFloat function

    while(table.rows.length >0 )       //we have to delete all the rows of previus result first
    {
        
        if(table.rows.length==1)    //if only 1 row is left then it will be deleted using index 0
            table.deleteRow(0);
        else
          table.deleteRow(1);
    }

  
   if(!Number.isInteger(num))       //checking if input is integer or not
        alert("Enter Integer only!!");
   else
    {
        if(num>0)                   //checking for positive integers
        {
            for(count=1;count <= num;count++)
            {
                var row:HTMLTableRowElement=table.insertRow();      //for inserting  a row
                var cell:HTMLTableDataCellElement=row.insertCell();    //for inserting  a cell
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.value=num.toString();  
                cell.appendChild(text); //to put value of text in the cell

                var cell:HTMLTableDataCellElement=row.insertCell();
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.value="*";
                cell.appendChild(text);

                var cell:HTMLTableDataCellElement=row.insertCell();
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.value=count.toString();
                cell.appendChild(text);

                var cell:HTMLTableDataCellElement=row.insertCell();
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.value="=";
                cell.appendChild(text);

                var cell:HTMLTableDataCellElement=row.insertCell();
                var text:HTMLInputElement=document.createElement("input");
                text.type="text";
                text.style.textAlign="Center";
                text.value=(count*num).toString();
                cell.appendChild(text);
            }
        }
        else
            alert("Enter Positive Integer only!!");
    }


}