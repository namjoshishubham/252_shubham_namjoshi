function mul() {
    var input = document.getElementById("n1");
    var table = document.getElementById("t1");
    var count = 1;
    var num = +input.value; //+is a shortcut for parseFloat function
    while (table.rows.length > 0) //we have to delete all the rows of previus result first
     {
        if (table.rows.length == 1) //if only 1 row is left then it will be deleted using index 0
            table.deleteRow(0);
        else
            table.deleteRow(1);
    }
    if (!Number.isInteger(num))
        alert("Enter Integer only!!");
    else {
        if (num > 0) {
            for (count = 1; count <= num; count++) {
                var row = table.insertRow();
                var cell = row.insertCell();
                var text = document.createElement("input");
                text.type = "text";
                text.style.textAlign = "Center";
                text.value = num.toString();
                cell.appendChild(text);
                var cell = row.insertCell();
                var text = document.createElement("input");
                text.type = "text";
                text.style.textAlign = "Center";
                text.value = "*";
                cell.appendChild(text);
                var cell = row.insertCell();
                var text = document.createElement("input");
                text.type = "text";
                text.style.textAlign = "Center";
                text.value = count.toString();
                cell.appendChild(text);
                var cell = row.insertCell();
                var text = document.createElement("input");
                text.type = "text";
                text.style.textAlign = "Center";
                text.value = "=";
                cell.appendChild(text);
                var cell = row.insertCell();
                var text = document.createElement("input");
                text.type = "text";
                text.style.textAlign = "Center";
                text.value = (count * num).toString();
                cell.appendChild(text);
            }
        }
        else
            alert("Enter Positive Integer only!!");
    }
}
//# sourceMappingURL=mtable.js.map