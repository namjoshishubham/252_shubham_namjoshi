function check_point()
{   //first cathing all the text values here
    var x1_t:HTMLInputElement=<HTMLInputElement>document.getElementById("x1");
    var y1_t:HTMLInputElement=<HTMLInputElement>document.getElementById("y1");
    var x2_t:HTMLInputElement=<HTMLInputElement>document.getElementById("x2");
    var y2_t:HTMLInputElement=<HTMLInputElement>document.getElementById("y2");
    var x3_t:HTMLInputElement=<HTMLInputElement>document.getElementById("x3");
    var y3_t:HTMLInputElement=<HTMLInputElement>document.getElementById("y3");
    var x_t:HTMLInputElement=<HTMLInputElement>document.getElementById("x");
    var y_t:HTMLInputElement=<HTMLInputElement>document.getElementById("y");

    //validation for empty input
    if(x1_t.value=="" || y1_t.value=="" || x2_t.value=="" || y2_t.value=="" || x3_t.value=="" || y3_t.value=="" || x_t.value=="" ||y_t.value=="")
        alert("Enter all the values!!");
    else
    {
        var x1:number= +x1_t.value;
        var y1:number= +y1_t.value;
        var x2:number= +x2_t.value;
        var y2:number= +y2_t.value;
        var x3:number= +x3_t.value;
        var y3:number= +y3_t.value;
        var x:number= +x_t.value;
        var y:number= +y_t.value;

        //validation for accepting only numbers
        if(isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2) || isNaN(x3) || isNaN(y3) || isNaN(x) ||isNaN(y))
            alert("Enter Numbers only!!!");
        else
        {
            var a_abc:number=0;
            var a_pab:number=0;
            var a_pbc:number=0;
            var a_pac:number=0;
            var sum:number=0;
            
            a_abc=Math.abs((x1 * (y2 - y3) + x2* (y3 - y1 ) + x3* (y1 - y2))/2);
            a_pab=Math.abs((x * (y1 - y2)+ x1 * (y2 - y)+ x2 * (y - y1))/2);
            a_pbc=Math.abs((x * (y2 - y3)+ x2 * (y3 - y)+ x3 * (y - y2))/2);
            a_pac=Math.abs((x * (y1 - y3)+ x1 * (y3 - y)+ x3 * (y - y1))/2);
            sum=a_pab + a_pbc + a_pac;

            if(Math.abs(a_abc-sum) < 0.0000001)
                document.getElementById("ans").innerHTML="<b><u>The Point lies INSIDE the triangle</u></b>";
            else
                document.getElementById("ans").innerHTML="<b><u>The Point lies OUTSIDE the triangle</u></b>";
        }
    }
}